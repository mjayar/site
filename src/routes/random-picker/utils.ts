export function extractArrayFromString(str: string, delim: string = '\n'): string[] {
    return str
        .trim()
        .split(delim)
        .map((item) => item.trim())
        .filter((item) => item !== '');
}

export function joinArrayToString(arr: string[], delim: string = '\n'): string {
    return arr
        .map((item) => item.trim())
        .filter((item) => item !== '')
        .join(delim);
}

export function shuffleArray<T>(arr: T[]): T[] {
    const copy = [...arr];
    for (let i = copy.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [copy[i], copy[j]] = [copy[j], copy[i]];
    }
    return copy;
}
