export function drawItem(ctx: CanvasRenderingContext2D, text: string, data: any, i: number) {
    const COLORS = ['#F44336', '#E91E63', '#9C27B0', '#673AB7', '#3F51B5'];
    const { arc, cx, cy, rotation } = data;

    ctx.beginPath();
    ctx.moveTo(cx, cy);
    ctx.arc(cx, cy, 470, arc * i + rotation, arc * (i + 1) + rotation);
    ctx.lineTo(cx, cy);
    ctx.fillStyle = COLORS[i % COLORS.length];
    ctx.fill();

    ctx.font = '32px "Geist Sans"';
    ctx.fillStyle = '#FFFFFF';
    ctx.save();
    ctx.translate(cx, cy);
    ctx.rotate(arc * i + rotation + arc / 2);
    ctx.fillText(text, Math.cos(arc * i + rotation) + 475 - 150, Math.sin(arc * i + rotation));
    ctx.restore();
}

export function drawOverlay(ctx: CanvasRenderingContext2D, cx: number, cy: number) {
    const CENTER_RADIUS = 30;
    const OUTER_RADIUS = 475;
    const OVERLAY_COLOR = '#FFFFFF';

    ctx.beginPath();
    ctx.arc(cx, cy, CENTER_RADIUS, 0, 2 * Math.PI);
    ctx.fillStyle = OVERLAY_COLOR;
    ctx.fill();

    ctx.beginPath();
    ctx.arc(cx, cy, OUTER_RADIUS, 0, 2 * Math.PI);
    ctx.strokeStyle = OVERLAY_COLOR;
    ctx.lineWidth = 10;
    ctx.stroke();

    ctx.beginPath();
    ctx.moveTo(cx + OUTER_RADIUS - 50, cy);
    ctx.lineTo(cx + OUTER_RADIUS + 100, cy + 30);
    ctx.lineTo(cx + OUTER_RADIUS + 100, cy - 30);
    ctx.fillStyle = OVERLAY_COLOR;
    ctx.fill();
}
